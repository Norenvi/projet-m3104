
<!DOCTYPE html>
<html>
<head>
	<title>Joueurs</title>
	<link rel="stylesheet" type="text/css" href="./style.css" />
	<?php session_start();
	include('header.php');
	if(!(isset($_SESSION['estConnecte']))){ 
		$_SESSION['estConnecte']=0;
	}
	 ?>
</head>
<body>
	<div id="contenu_page">
		<?php
		if($_SESSION['estConnecte']!=1){
			echo "<h2>Erreur : Vous devez vous connecter pour consulter le site</h2><br/><a href=\"index.php\">Page de connexion</a>";
		} else {
		?>
		<h1> Affichage des joueurs </h1>

<?php
include 'database.php';
global $db;
$q = $db->prepare('SELECT * FROM joueur ORDER BY nom');
$q->execute();?>

		<table>
		<tr>
			<td>Photo</td>
			<td>Nom</td>
			<td>Prénom</td>
			<td>Date de Naissance (AAAA-MM-JJ)</td>
			<td>Taille (en m)</td>
			<td>Poids (en kg)</td>
			<td>Poste Préféré</td>
			<td>Statut</td>
			<td>Commentaire</td>
			<td>Numéro de Licence</td>
			<td id="colonne_hidden" name="modification"></td>
			<td id="colonne_hidden" name="suppression"></td>
		</tr>
	<?php
		while ($a = $q->fetch()) {
	?>
	
	<tr>
		<td><img src="img/<?=$a['photo']?>" height="100" alt="<?=$a['photo']?>"></td>
		<td><?= $a['nom'] ?></td>
		<td><?= $a['prenom'] ?></td>
		<td><?= $a['date_n'] ?></td>
		<td><?= $a['taille'] ?></td>
		<td><?= $a['poids'] ?></td>
		<td><?= $a['postepref'] ?></td>
		<td><?= $a['statut'] ?></td>
		<td><?= $a['commentaire'] ?></td>
		<td><?= $a['numlicence'] ?></td>
		<td id="colonne_hidden"><a href="modificationjoueur.php?numlicence=<?= $a['numlicence'] ?>"><input type="submit" value="Modifier"/></a></td>
		<td id="colonne_hidden"><a class="del" href="suppressionjoueur.php?numlicence=<?= $a['numlicence'] ?>"><input type="submit" value="Supprimer" id="input_suppr"/></a></td>
	</tr>

	<?php


	}
}

?>
	</table>
</div>
</body>
</html>