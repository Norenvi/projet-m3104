<!DOCTYPE html>
<html>
<head>
	<title>Ajout d'un match</title>
	<link rel="stylesheet" href="./style.css" />
	<?php session_start();
	include('header.php');
	if(!(isset($_SESSION['estConnecte']))){ 
		$_SESSION['estConnecte']=0;
	}
		global $db;
		include 'database.php'; ?>
</head>
<meta charset="utf-8">
<body>
	<div id="contenu_page">
		<?php
		if($_SESSION['estConnecte']!=1){
			echo "<h2>Erreur : Vous devez vous connecter pour consulter le site</h2><br/><a href=\"index.php\">Page de connexion</a>";
		} else { ?>
		<h1>Ajout d'un match</h1>
		<h3> Veuillez entrer les données du match que vous voulez créer</h3>
		<form action="ajoutmatch.php" method="post">
			Date du match : <input type="date" name="date_m" value=""><br/>
			Heure du match : <input type="time" name="heure_m" value=""><br/>
			Nom de l'équipe adverse : <input type="text" name="nomadv" value=""><br/>
			<div id="lieumatch">
				Lieu du match : 
					<input type="radio" id="domicile" name="lieu" value="Domicile" checked><label for="domicile">Domicile</label>			
					<input type="radio" id="exterieur" name="lieu" value="Extérieur"><label for="exterieur">Extérieur</label> <br/> <br/> <br/>
			</div>
				Score final : <br/> <br/>
				Notre équipe : <input type="number" step="1" name="score_equipe" value="0"> <br/>
				Adversaires : <input type="number" step="1" name="score_adv" value="0"> <br/>
				<?php
				$request = $db->prepare('SELECT * FROM joueur WHERE statut =:actif ORDER BY nom');
				$request->bindValue(':actif',"Actif",PDO::PARAM_STR);
				?>
				<h3> Choix des joueurs pour le match (seuls les actifs sont affichés) :</h3>
				<table>
				<tr>
					<td>Photo</td>
					<td>Nom</td>
					<td>Prénom</td>
					<td>Taille (en m)</td>
					<td>Poids (en kg)</td>
					<td>Poste Préféré</td>
					<td>Commentaire</td>
					<td name="Note">Note du joueur</td>
					<td id="colonne_hidden" name="Remplissage"></td>
					<td id="colonne_hidden" name="Titulaire"></td>
					<td id="colonne_hidden" name="Remplaçant"></td>
				</tr>
			<?php
				$request->execute();
				$nbroles = 0;
				$numlicence = 0;
				while ($a = $request->fetch()) {
			?>
			
			<tr>
				<td><img src="img/<?=$a['photo']?>" height="100" alt="<?=$a['photo']?>"></td>
				<td><?= $a['nom'] ?></td>
				<td><?= $a['prenom'] ?></td>
				<td><?= $a['taille'] ?></td>
				<td><?= $a['poids'] ?></td>
				<td><?= $a['postepref'] ?></td>
				<td><?= $a['commentaire'] ?></td>
				<td><input type="number" step="1" min="0" max="5" name="note<?=$nbroles?>"></td>
				<td id="colonne_hidden"><input type="hidden" name="titulaire<?=$nbroles?>" value="null"></td>
				<td id="colonne_hidden"><input type="radio" id="titulaire" name="titulaire<?=$nbroles?>" value="Titulaire"><label for="Titulaire">Titulaire</label></td>
				<td id="colonne_hidden"><input type="radio" id="titulaire" name="titulaire<?=$nbroles?>" value="Remplacant"><label for="Titulaire">
				Remplaçant</label></td>
				<input type="hidden" name="numlicence<?=$nbroles?>" value="<?=$a['numlicence']?>">
				<input type="hidden" name="nbroles" value="<?= $nbroles += 1; ?>">
			<?php 
			} ?>
			</tr>
			<div id="validerreset">
				<input type="submit" name="valider" value="Valider">
			</div>
		</form>
	</div>
<?php } ?>
</body>
</html>