<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="./style.css" />
	<link rel="icon" type="image/png" sizes="16x16" href="logo_onglet.png">
</head>
<meta charset="utf-8">
<body>
	<header>
		<div id="header_logo_and_title">
			<img src="logo.png" height="150" id="logo">
			<h1 id="title_header">Gestionnaire de matchs - Équipe de France </h1>
		</div>
		<div id="header_bar">
        </div>
	</header>