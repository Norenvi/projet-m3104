<!DOCTYPE html>
<html>
<head>
	<title>Modif Joueur</title>
	<link rel="stylesheet" href="./style.css" />
	<?php include 'header.php'; ?>
	<?php include 'database.php'; ?>
</head>
<body>
	<div id="contenu_page">
		<h1>Modification d'un joueur</h1>
		<form action="modification.php" method="post">
			<?php
			global $db;
			$request = $db->prepare('SELECT * FROM joueur WHERE numlicence =:numlicence');
			$request->bindValue(':numlicence',$_GET['numlicence'],PDO::PARAM_INT);
			$request->execute();
			$recupJoueur = $request->fetch();
			?>
				Nom : <input type="text" name="nom" value="<?= $recupJoueur['nom'] ?>">
				Prenom : <input type="text" name="prenom" value="<?= $recupJoueur['prenom'] ?>"><br>
				Date de Naissance : <input type="date" name="date_n" value="<?= $recupJoueur['date_n'] ?>"><br>
				Taille : <input type="number" step="0.01" name="taille" value="<?= $recupJoueur['taille'] ?>">
				Poids : <input type="number" step="0.01" name="poids" value="<?= $recupJoueur['poids'] ?>"><br>
				Poste Préféré : <select name="postepref" id="postepref">
		    			<option value="<?= $recupJoueur['postepref'] ?>"><?= $recupJoueur['postepref'] ?></option>
		   				<option value="Gardien">Gardien</option>
		    			<option value="Ailier Gauche">Ailier Gauche</option>
						<option value="Ailier Droit">Ailier Droit</option>
		    			<option value="Arrière Gauche">Arrière Gauche</option>
		    			<option value="Arrière Droit">Arrière Droit</option>
		    			<option value="Demi-Centre">Demi-Centre</option>
		    			<option value="Pivot">Pivot</option>
						</select><br>
				Statut : <select name="statut" id="statut">
		    			<option value="<?= $recupJoueur['statut'] ?>"><?= $recupJoueur['statut'] ?></option>
		   				<option value="Actif">Actif</option>
		    			<option value="Blessé">Blessé</option>
						<option value="Suspendu">Suspendu</option>
		    			<option value="Absent">Absent</option>
		    			</select></br>
		    			<img class="img_dans_modif" src="<?= "./img/".$recupJoueur['photo'] ?>"> <br/>
				Commentaire : <input type="text" name="commentaire" value="<?= $recupJoueur['commentaire'] ?>"></br>
				Numéro de Licence : <input type="number" name="numlicence" value="<?= $recupJoueur['numlicence'] ?>"></br>
				<input type="submit" name="valider" value="Valider">
		</form>
		<form action="modificationjoueur.php">
			<input type="submit" name="" value="reset">
		</form>
	</div>
	
</body>
</html>