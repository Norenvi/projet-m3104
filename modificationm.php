<!DOCTYPE html>
<html>
<head>
	<title>Modif Match</title>
	<link rel="stylesheet" href="./style.css" />
	<?php include 'header.php'; ?>
	<?php include 'database.php'; ?>
</head>
<body>
<?php
	global $db;

	$request = $db->prepare('UPDATE match_equipe SET date_m=:date_m, heure_m=:heure_m, nomadv=:nomadv, lieu=:lieu, score_equipe=:score_equipe, score_adv=:score_adv WHERE id_match =:id_match LIMIT 1');

	$request->bindValue(':id_match', $_POST['id_match']);
	$request->bindValue(':date_m', $_POST['date_m']);
	$request->bindValue(':heure_m', $_POST['heure_m']);
	$request->bindValue(':nomadv', $_POST['nomadv']);
	$request->bindValue(':lieu', $_POST['lieu']);
	$request->bindValue(':score_equipe', $_POST['score_equipe']);
	$request->bindValue(':score_adv', $_POST['score_adv']);

	$estExecutee = $request->execute();

	$estExecutee2 = false;
	$j = 0;
	$nbjoueursreels = 0;
	$i = 0;

for ($i; $i < $_POST['nbroles']; $i++) {
	$role = "titulaire".$i;
	if($_POST[$role] != "null"){
		$nbjoueursreels += 1;
	}
}


if($nbjoueursreels <= 12) {
	for ($j; $j < $_POST['nbroles']; $j++) {
		$role = "titulaire".$j;
		$note = "note".$j;
		if($_POST[$role] != "null" && $_POST[$note] != null) {
			$request = $db->prepare('SELECT id_match FROM match_equipe WHERE date_m=:date_m AND heure_m=:heure_m');
			$request->bindValue(':date_m', $_POST['date_m']);
			$request->bindValue(':heure_m', $_POST['heure_m']);
			$request->execute();
			$id_match_array = $request->fetch();
			$id_match = $id_match_array[0];
			$numlicence = "numlicence".$j;
			$request = $db->prepare('INSERT INTO match_role_joueur(numlicence,id_match,role,note) VALUES (:numlicence,:id_match,:role,:note) ON DUPLICATE KEY UPDATE role=:role, note=:note');
			$request->bindValue(':numlicence', $_POST[$numlicence]);
			$request->bindValue(':role', $_POST[$role]);
			$request->bindValue(':id_match', $id_match);
			$request->bindValue(':note', $_POST[$note]);
			$estExecutee2 = $request->execute();
		} else if ($_POST[$role] != "null") {
			$request = $db->prepare('SELECT id_match FROM match_equipe WHERE date_m=:date_m AND heure_m=:heure_m');
			$request->bindValue(':date_m', $_POST['date_m']);
			$request->bindValue(':heure_m', $_POST['heure_m']);
			$request->execute();
			$id_match_array = $request->fetch();
			$id_match = $id_match_array[0];
			$numlicence = "numlicence".$j;
			$request = $db->prepare('INSERT INTO match_role_joueur(numlicence,id_match,role) VALUES (:numlicence,:id_match,:role) ON DUPLICATE KEY UPDATE role=:role');
			$request->bindValue(':numlicence', $_POST[$numlicence]);
			$request->bindValue(':role', $_POST[$role]);
			$request->bindValue(':id_match', $id_match);
			$estExecutee2 = $request->execute();
				
			}
		}

} else if ($nbjoueursreels > 12) {
		echo "<h3> Il y a trop de joueurs attribués à ce match (Vous devez choisir 12 joueurs, 7 titulaires et 5 remplaçants)<br/><br/> Les attributions ont été réinitialisées</h3>";
	} else {
		
	}

if($estExecutee || $estExecutee && $estExecutee2) {
	echo "<h1> Match modifié</h1><h2> La modification du match s'est effectuée avec succès</h2>";
}
?>	