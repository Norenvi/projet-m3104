<!DOCTYPE html>
<html>
<head>
	<title>Modif Joueur</title>
	<link rel="stylesheet" href="./style.css" />
	<?php include 'header.php'; ?>
	<?php include 'database.php'; ?>
</head>
<body>
<?php
	global $db;
	$request = $db->prepare('UPDATE joueur SET nom=:nom, prenom=:prenom, date_n=:date_n, taille=:taille, poids=:poids, postepref=:postepref, statut=:statut, photo=:photo, commentaire=:commentaire, numlicence=:numlicence WHERE numlicence =:numlicence LIMIT 1');

	$request->bindValue(':nom', $_POST['nom']);
	$request->bindValue(':prenom', $_POST['prenom']);
	$request->bindValue(':date_n', $_POST['date_n']);
	$request->bindValue(':taille', $_POST['taille']);
	$request->bindValue(':poids', $_POST['poids']);
	$request->bindValue(':postepref', $_POST['postepref']);
	$request->bindValue(':statut', $_POST['statut']);
	$request->bindValue(':photo', $_POST['photo']);
	$request->bindValue(':commentaire', $_POST['commentaire']);
	$request->bindValue(':numlicence', $_POST['numlicence']);

	$estExecutee = $request->execute();

	if($estExecutee) {
		echo "<h2> La modification du joueur s'est opérée avec succès</h2>";
	}
?>	