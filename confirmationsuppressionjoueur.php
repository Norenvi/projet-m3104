<!DOCTYPE html>
<html>
<head>
	<title>Suppression</title>
	<link rel="stylesheet" type="text/css" href="./style.css" />
	<?php include('header.php'); ?>
</head>
<body>

<?php
	include 'database.php';
	global $db;
	$request = $db->prepare('DELETE FROM joueur WHERE numlicence =:numlicence LIMIT 1');
	$request->bindValue(':numlicence',$_POST['numlicence'],PDO::PARAM_INT);
	$estExecutee = false;
	
	if($_POST['valider'] == "Oui") {
		$estExecutee = $request->execute();
	} else {
		header('Location: affichagejoueur.php');
	}

	if($estExecutee) {
		echo "<h1> Suppression effecutée </h1><h2>La suppression a été effectuée avec succès</h2>";
	}
	?>
</body>
</html>