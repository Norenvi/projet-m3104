<?php
$img = isset($_FILES['img']) ? $_FILES['img'] : NULL;
$dossier = 'upload/';
$fichier = basename($img['name']);
$image = $img['tmp_name']; 
$taille_maxi = 10000000;
$taille = filesize($img['tmp_name']);
$extensions = array('.png', '.gif', '.jpg', '.jpeg');
$extension = strrchr($img['name'], '.'); 

if(!in_array($extension, $extensions)) {
     $erreur = 'Vous devez uploader un fichier de type png, gif, jpg, jpeg';
}

if($taille>$taille_maxi) {
     $erreur = 'Le fichier est trop volumineux';
}

if(!isset($erreur)) {

     if(move_uploaded_file($img['tmp_name'], $dossier . $fichier)) { 
          //Si la fonction renvoie TRUE, c'est que ça a fonctionné 
     }
     else //Sinon (la fonction renvoie FALSE).
     {
          echo 'Le fichier est trop volumineux pour l\'upload';
     }
}
else
{
     echo $erreur;
}

?>