<!DOCTYPE html>
<html>
<head>
	<title>Identification</title>
	<link rel="stylesheet" type="text/css" href="./style.css" />
	<?php session_start();
	if(!(isset($_SESSION['estConnecte']))){ 
		$_SESSION['estConnecte']=0;
	}
	if($_SESSION['estConnecte']==1){
		include ('header.php');
	} else {
	include('headernonconnecte.php'); }?>
</head>
<body>
	<?php
	include 'database.php';
	global $db;
	$login = isset($_POST['mail']) ? $_POST['mail'] : null;
	$password = isset($_POST['mdp']) ? $_POST['mdp'] : null;
	$request = $db->prepare('SELECT id_user FROM user WHERE mail =:mail AND mdp =:mdp');
	$request->execute([
		':mail' => $login,
		':mdp' => $password
	]);
	$request->fetch();

	?>
		<div id="contenu_page">
				<h1> Connexion </h1>
				<h3> Pour accéder au site, vous devez être connecté </h3>
				<h3> (Login: entraineurequipe@gmail.com <br/> Mot de Passe : entraineur1234) </h3>
				<form method="post" action="index.php">
					<label for="login">Adresse mail : </label><input type="text" name="mail" placeholder="Login"> <br/>
					<label for="password">Mot de passe : </label><input type="password" name="mdp" placeholder="Password"> 
					<br/>
					<input type="submit" name="Valider">
				</form>
				<br/>
		<?php 
		if($request->rowCount() == 1){
			$_SESSION['estConnecte']=1;
		} 
		if($_SESSION['estConnecte']==1){
			echo "<em> Connexion au site réussie, vous pouvez maintenant naviguer : </em><a href=\"affichagematch.php\">Accéder au site</a>";
		} else {
			echo "<em> Identifiants incorrects, veuillez réessayer </em>";
		}
		?>
		</div>
	
</body>
</html>