<!DOCTYPE html>
<html>
<head>
	<title>Inscription</title>
	<link rel="stylesheet" href="./style.css" />
	<?php session_start();
	include('header.php');
	if(!(isset($_SESSION['estConnecte']))){ 
		$_SESSION['estConnecte']=0;
	}
	?>
</head>
<meta charset="utf-8">
<body>
	<div id="contenu_page">
		<?php
		if($_SESSION['estConnecte']!=1){
			echo "<h2>Erreur : Vous devez vous connecter pour consulter le site</h2><br/><a href=\"index.php\">Page de connexion</a>";
		} else { ?>
		<h1>Inscription du joueur</h1>
		<h3> Veuillez entrer les informations nécessaires à l'inscription du joueur :</h3>
		<form action="ajoutjoueur.php" method="post">
			Nom : <input type="text" name="nom" value="">
			Prenom : <input type="text" name="prenom" value=""><br>
			Date de Naissance : <input type="date" name="date_n" value=""><br>
			Taille : <input type="number" step="0.01" name="taille" value="0">
			Poids : <input type="number" step="0.01" name="poids" value="0"><br>
			Poste Préféré : <select name="postepref" id="postepref">
	    			<option value="">Choisissez le poste</option>
	   				<option value="Gardien">Gardien</option>
	    			<option value="Ailier Gauche">Ailier Gauche</option>
					<option value="Ailier Droit">Ailier Droit</option>
	    			<option value="Arrière Gauche">Arrière Gauche</option>
	    			<option value="Arrière Droit">Arrière Droit</option>
	    			<option value="Demi-Centre">Demi-Centre</option>
	    			<option value="Pivot">Pivot</option>
					</select><br>
			Statut : <select name="statut" id="statut">
	    			<option value="">Choisissez le statut</option>
	   				<option value="Actif">Actif</option>
	    			<option value="Ailier Gauche">Blessé</option>
					<option value="Ailier Droit">Suspendu</option>
	    			<option value="Arrière Gauche">Absent</option>
	    			</select></br>
			Photo : <input type="file" name="photo" accept="image/jpg, image/gif image/jpeg image/png"><br/>
			Commentaire : <input type="text" name="commentaire" value=""><br/>
			Numéro de Licence : <input type="number" name="numlicence" value=""><br/><br/>
			<input type="submit" name="valider" value="Valider">
			<input type="submit" name="Reset" value="Reset">
		</form>
	</div>
<?php } ?>
</body>
</html>