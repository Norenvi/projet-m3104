<!DOCTYPE html>
<html>
<head>
	<title>Suppression</title>
	<link rel="stylesheet" type="text/css" href="./style.css" />
	<?php include('header.php'); ?>
</head>
<body>

<?php
	include 'database.php';
	global $db;
	$request = $db->prepare('DELETE FROM match_equipe WHERE id_match=:id_match LIMIT 1');
	$request->bindValue(':id_match',$_POST['id_match'],PDO::PARAM_INT);
	$estExecutee = false;
	$request2 = $db->prepare('DELETE FROM match_role_joueur WHERE id_match=:id_match');
	$request2->bindValue(':id_match',$_POST['id_match'],PDO::PARAM_INT);
	$estExecutee2 = false;
	
	if($_POST['valider'] == "Oui") {
		$estExecutee = $request->execute();
		$estExecutee2 = $request2->execute();
	} else {
		header('Location: affichagematch.php');
	}

	if($estExecutee) {
		echo "<h1> Suppression effecutée </h1><h2>La suppression a été effectuée avec succès</h2>";
	}
	?>
</body>
</html>