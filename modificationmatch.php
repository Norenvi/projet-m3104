<!DOCTYPE html>
<html>
<head>
	<title>Modif Match</title>
	<link rel="stylesheet" href="./style.css" />
	<?php include 'header.php'; ?>
	<?php include 'database.php'; ?>
</head>
<body>
	<div id="contenu_page">
		<h1>Modification d'un match</h1>
		<form action="modificationm.php" method="post">
			<?php
			global $db;
			echo $_GET['id_match'];
			$request = $db->prepare('SELECT * FROM match_equipe WHERE id_match=:id_match');
			$request->bindValue(':id_match',$_GET['id_match'],PDO::PARAM_INT);
			$request->execute();
			$recupMatch = $request->fetch();
			?>
			<input id="id_match" name="id_match" type="hidden" value="<?= $_GET['id_match'] ?>">
			Date du match : <input type="date" name="date_m" value="<?= $recupMatch['date_m'] ?>"><br/>
			Heure du match : <input type="time" name="heure_m" value="<?= $recupMatch['heure_m'] ?>"><br/>
			Nom de l'équipe adverse : <input type="text" name="nomadv" value="<?= $recupMatch['nomadv'] ?>"><br/>
			<div id="lieumatch">
				Lieu du match : 
					<?php
					if ($recupMatch['lieu'] == "Domicile") { ?>
					<input type="radio" id="domicile" name="lieu" value="Domicile" checked><label for="domicile">Domicile</label>			
					<input type="radio" id="exterieur" name="lieu" value="Extérieur"><label for="exterieur">Extérieur</label> <br/> <br/> <br/>
				<?php } else { ?>
					<input type="radio" id="domicile" name="lieu" value="Domicile"><label for="domicile">Domicile</label>			
					<input type="radio" id="exterieur" name="lieu" value="Extérieur" checked><label for="exterieur">Extérieur</label> <br/> <br/> <br/>
				<?php } ?>
			</div>
				Score final : <br/> <br/>
				Notre équipe : <input type="number" step="1" name="score_equipe" value="<?= $recupMatch['score_equipe'] ?>"> <br/>
				Adversaires : <input type="number" step="1" name="score_adv" value="<?= $recupMatch['score_adv'] ?>"> <br/> <br/> <br/> <br/>
				<?php
				$request = $db->prepare('SELECT numlicence,photo,nom,prenom,taille,poids,postepref,commentaire FROM joueur WHERE statut=:actif ORDER BY nom');
				$request->bindValue(':actif',"Actif",PDO::PARAM_STR);
				$request->execute();

				?>
				<h3> Choix des joueurs pour le match (seuls les actifs sont affichés) :</h3>
				<table>
				<tr>
					<td>Photo</td>
					<td>Nom</td>
					<td>Prénom</td>
					<td>Taille (en m)</td>
					<td>Poids (en kg)</td>
					<td>Poste Préféré</td>
					<td>Commentaire</td>
					<td name="Note">Note du joueur</td>
					<td id="colonne_hidden" name="Remplissage"></td>
					<td id="colonne_hidden" name="Titulaire"></td>
					<td id="colonne_hidden" name="Remplaçant"></td>
				</tr>
			<?php

				$nbroles = 0;
				while ($recupJoueur = $request->fetch()) {
					$numlicence = $recupJoueur['numlicence'];
					$request2 = $db->prepare('SELECT role,note from match_role_joueur WHERE numlicence=:numlicence AND id_match=:id_match');
					$request2->bindValue(':numlicence',$numlicence,PDO::PARAM_INT);
					$request2->bindValue(':id_match',$_GET['id_match'],PDO::PARAM_INT);
					$request2->execute();
					$rolenote = $request2->fetch();
					
			?>
					<tr>
						<td><img src="img/<?=$recupJoueur['photo']?>" height="100"></td>
						<td><?= $recupJoueur['nom'] ?></td>
						<td><?= $recupJoueur['prenom'] ?></td>
						<td><?= $recupJoueur['taille'] ?></td>
						<td><?= $recupJoueur['poids'] ?></td>
						<td><?= $recupJoueur['postepref'] ?></td>
						<td><?= $recupJoueur['commentaire'] ?></td>
						<td><input type="number" step="1" min="0" max="5" name="note<?=$nbroles?>" value="<?=$rolenote['note']?>"></td>
						<?php 
						if ($rolenote['role'] == "Titulaire") { ?>
							<td id="colonne_hidden"><input type="hidden" name="titulaire<?=$nbroles?>" value="null"></td>
							<td id="colonne_hidden"><input type="radio" id="titulaire" name="titulaire<?=$nbroles?>" value="Titulaire" checked><label for="titulaire">Titulaire</label></td>
							<td id="colonne_hidden"><input type="radio" id="titulaire" name="titulaire<?=$nbroles?>" value="Remplacant">Remplaçant<label for="titulaire"></label>
							<input type="hidden" name="numlicence<?=$nbroles?>" value="<?= $recupJoueur['numlicence'] ?>">
							<input type="hidden" name="nbroles" value="<?= $nbroles += 1; ?>">
						<?php } else if($rolenote['role'] == "Remplacant"){ ?>
								<td id="colonne_hidden"><input type="hidden" name="titulaire<?=$nbroles?>" value="null"></td>
								<td id="colonne_hidden"><input type="radio" id="titulaire" name="titulaire<?=$nbroles?>" value="Titulaire"><label for="titulaire">Titulaire</label></td>
								<td id="colonne_hidden"><input type="radio" id="titulaire" name="titulaire<?=$nbroles?>" value="Remplacant" checked>Remplaçant<label for="titulaire"></label>
								<input type="hidden" name="numlicence<?=$nbroles?>" value="<?= $recupJoueur['numlicence'] ?>">
								<input type="hidden" name="nbroles" value="<?= $nbroles += 1; ?>">
						<?php } else { ?>
								<td id="colonne_hidden"><input type="hidden" name="titulaire<?=$nbroles?>" value="null"></td>
								<td id="colonne_hidden"><input type="radio" id="titulaire" name="titulaire<?=$nbroles?>" value="Titulaire"><label for="titulaire">Titulaire</label></td>
								<td id="colonne_hidden"><input type="radio" id="titulaire" name="titulaire<?=$nbroles?>" value="Remplacant"><label for="titulaire">Remplaçant</label></td>
								<input type="hidden" name="numlicence<?=$nbroles?>" value="<?= $recupJoueur['numlicence'] ?>">
								<input type="hidden" name="nbroles" value="<?= $nbroles += 1; ?>">
							<?php }
					} ?>
					</tr>
		<div id="validerreset">	
			<input type="submit" id="validermodif" name="valider" value="Valider">
		</form>
		</div>
	</div>
	
</body>
</html>