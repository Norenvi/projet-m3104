<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="./style.css" />
	<link rel="icon" type="image/png" sizes="16x16" href="logo_onglet.png">
</head>
<meta charset="utf-8">
<body>
	<header>
		<div id="header_logo_and_title">
			<img src="logo.png" height="150" id="logo">
			<h1 id="title_header">Gestionnaire de matchs - Équipe de France </h1>
		</div>
		<div id="header_bar">
			<li class="navigation__item"><a href="indexajoutjoueur.php" id="lien_header">Ajouter un joueur</a></li>
        	<li class="navigation__item"><a href="affichagejoueur.php" id="lien_header">Joueurs</a></li>
        	<li class="navigation__item"><a href="indexajoutmatch.php" id="lien_header">Ajouter un match</a></li>
        	<li class="navigation__item"><a href="affichagematch.php" id="lien_header">Matchs</a></li>
        	<li class="navigation__item"><a href="affichagestats.php" id="lien_header">Statistiques</a></li>
        	<?php if($_SESSION['estConnecte']==1) { ?>
        	<li class="navigation__item"><a href="deconnexion.php" id="lien_header_deconnexion">Se déconnecter</a></li>
        	<?php } ?>
        </div>
	</header>
