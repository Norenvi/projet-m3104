<!DOCTYPE html>
<html>
<head>
	<title>Suppression</title>
	<link rel="stylesheet" type="text/css" href="./style.css" />
	<?php session_start();
	include('header.php'); ?>
</head>
<body>

<?php
	include 'database.php';
	global $db;
	if($_POST['valider'] == "Oui") {
		unset($_SESSION['estConnecte']);
		echo "<h3>Vous êtes désormais déconnecté du site</h3>";
	} else {
		header('Location: affichagematch.php');
	}
	?>
</body>
</html>