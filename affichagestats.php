<!DOCTYPE html>
<html>
<head>
	<title>Statistiques</title>
	<link rel="stylesheet" href="./style.css" />
	<?php session_start();
	include('header.php');
	if(!(isset($_SESSION['estConnecte']))){ 
		$_SESSION['estConnecte']=0;
	}
	?>
</head>
<meta charset="utf-8">
<body>

	<?php
	if($_SESSION['estConnecte']!=1){
			echo "<div id=\"contenu_page\"><h2>Erreur : Vous devez vous connecter pour consulter le site</h2><br/><a href=\"index.php\">Page de connexion</a></div>";
		} else {

		include 'database.php';
		global $db;

		$request = $db->prepare('SELECT COUNT(id_match) FROM match_equipe WHERE score_equipe>score_adv');
		$request->execute();
		$temp = $request->fetch();
		$nbmatchsgagnes = $temp[0];

		$request2 = $db->prepare('SELECT COUNT(id_match) FROM match_equipe WHERE score_equipe<score_adv');
		$request2->execute();
		$temp2 = $request2->fetch();
		$nbmatchsperdus = $temp2[0];

		$request3 = $db->prepare('SELECT COUNT(id_match) FROM match_equipe WHERE score_equipe=score_adv');
		$request3->execute();
		$temp3 = $request3->fetch();
		$nbmatchsegalite = $temp3[0];

		$request4 = $db->prepare('SELECT COUNT(id_match) FROM match_equipe');
		$request4->execute();
		$temp4 = $request4->fetch();
		$nbmatchs = $temp4[0];

		$request5 = $db->prepare('SELECT numlicence,nom,prenom,photo,statut,postepref FROM joueur ORDER BY nom');
		$request5->execute();
	?>

	<div id="contenu_page">
		<h1>Statistiques </h1>
		<h2>Sur les matchs : </h2>
		<div id="box_entouree"> 
			Nombre de matchs gagnés : <?=$nbmatchsgagnes;?> (<?=round(($nbmatchsgagnes/$nbmatchs)*100) ?>%)<br/>
			Nombre de matchs perdus : <?=$nbmatchsperdus;?> (<?=round(($nbmatchsperdus/$nbmatchs)*100) ?>%)<br/>
			Nombre de matchs ex-aequo : <?=$nbmatchsegalite;?> (<?=round(($nbmatchsegalite/$nbmatchs)*100) ?>%)<br/>
		</div>
		<h2>Sur les joueurs : </h2>
		<div id="box_entouree">
			<table>
				<tr>
					<td> Nom </td>
					<td> Photo </td>
					<td> Statut </td>
					<td> Poste Préféré </td>
					<td> Nombre de sélections en tant que titulaire </td>
					<td> Nombre de sélections en tant que remplaçant </td>
					<td> Note d'évaluation moyenne </td>
					<td> Pourcentages de matchs gagnés </td>
				</tr>
				<?php while($recupJoueur = $request5->fetch()){ ?>
				<tr>
					<td> <?=$recupJoueur['nom']." ".$recupJoueur['prenom']?> </td>
					<td> <img src="img/<?=$recupJoueur['photo']?>" height="100" alt="<?=$recupJoueur['photo']?>"> </td>
					<td> <?=$recupJoueur['statut']?> </td>
					<td> <?=$recupJoueur['postepref']?> </td>
					<td>  <?php $request6 = $db->prepare('SELECT DISTINCT COUNT(numlicence) FROM match_role_joueur WHERE numlicence=:numlicence  AND role="Titulaire"');
						$request6->bindValue(':numlicence', $recupJoueur['numlicence']);
						$request6->execute();
						$temp6 = $request6->fetch();
						$nbmatchstitulaire = $temp6[0];
						echo $nbmatchstitulaire; ?> </td>
					<td> <?php $request6 = $db->prepare('SELECT COUNT(numlicence) FROM match_role_joueur WHERE numlicence=:numlicence  AND role="Remplacant"');
						$request6->bindValue(':numlicence', $recupJoueur['numlicence']);
						$request6->execute();
						$temp6 = $request6->fetch();
						$nbmatchsremplacant = $temp6[0];
						echo $nbmatchsremplacant; ?> </td>
					<td> <?php $request7 = $db->prepare('SELECT SUM(note) FROM match_role_joueur WHERE numlicence=:numlicence');
						$request7->bindValue(':numlicence', $recupJoueur['numlicence']);
						$request7->execute();
						$temp7 = $request7->fetch();
						$moyennenotes = round(($temp7[0]/$nbmatchs),2);
						echo $moyennenotes; ?> </td>
					<td> <?php $request8 = $db->prepare('SELECT COUNT(match_role_joueur.numlicence) FROM match_role_joueur,match_equipe WHERE match_role_joueur.numlicence=:numlicence AND match_equipe.id_match=match_role_joueur.id_match AND match_equipe.score_equipe>match_equipe.score_adv');
						$request8->bindValue(':numlicence', $recupJoueur['numlicence']);
						$request8->execute();
						$temp8 = $request8->fetch();
						$nbmatchsgagnes = $temp8[0];
						echo round(($nbmatchsgagnes/$nbmatchs)*100); ?>% </td>
				</tr>
			<?php } } ?>
			</table>
		</div>


</body>
</html>