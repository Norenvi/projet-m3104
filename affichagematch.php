
<!DOCTYPE html>
<html>
<head>
	<title>Matchs</title>
	<link rel="stylesheet" type="text/css" href="./style.css" />
	<?php session_start();
	include('header.php');
	if(!(isset($_SESSION['estConnecte']))){ 
		$_SESSION['estConnecte']=0;
	}
	?>
</head>
<body>
	<div id="contenu_page">
		<?php
		if($_SESSION['estConnecte']!=1){
			echo "<h2>Erreur : Vous devez vous connecter pour consulter le site</h2><br/><a href=\"index.php\">Page de connexion</a>";
		} else {
		?>
		<h1> Matchs </h1>

<?php
include 'database.php';
global $db;
$request = $db->prepare('SELECT * FROM match_equipe ORDER BY date_m DESC');
$request->execute();?>

	<?php
		while ($a = $request->fetch()) {
	?>
			<table>
		<tr>
			<td>Date (AAAA/MM/JJ)</td>
			<td>Heure</td>
			<td>Nom Adversaire</td>
			<td>Score Équipe</td>
			<td>Score Adversaire</td>
			<td>Lieu du match</td>
			<td id="colonne_hidden" name="modification"></td>
			<td id="colonne_hidden" name="suppression"></td>
		</tr>
	
	<tr>
		<td><?= $a['date_m'] ?></td>
		<td><?= $a['heure_m'] ?></td>
		<td><?= $a['nomadv'] ?></td>
		<td><?= $a['score_equipe'] ?></td>
		<td><?= $a['score_adv'] ?></td>
		<td><?= $a['lieu'] ?></td>
		<td id="colonne_hidden"><a href="modificationmatch.php?id_match=<?= $a['id_match'] ?>"><input type="submit" value="Modifier"/></a></td>
		<td id="colonne_hidden"><a class="del" href="suppressionmatch.php?id_match=<?= $a['id_match'] ?>"><input type="submit" value="Supprimer" id="input_suppr"/></a></td>
	</tr>

	<?php
		$id_match = $a['id_match'];
		$request2 = $db->prepare('SELECT numlicence,role,note FROM match_role_joueur WHERE id_match=:id_match ORDER BY numlicence');
		$request2->bindValue(':id_match',$id_match,PDO::PARAM_INT);
		$request2->execute();

		while ($rolenote = $request2->fetch()) {
			$numlicence = $rolenote['numlicence'];
			$request3 = $db->prepare('SELECT photo,nom,prenom FROM joueur WHERE numlicence=:numlicence ORDER BY numlicence');
			$request3->bindValue(':numlicence',$numlicence,PDO::PARAM_INT);
			$request3->execute();

			while($recupJoueur = $request3->fetch()){
				?>
				<table id="affichematch">
					<tr>
						<td><img id="img_dans_affichematch" src="img/<?=$recupJoueur['photo']?>" height="100" alt="<?=$recupJoueur['photo']?>"></td>
					</tr>
					<tr>
						<td><?=$recupJoueur['nom']?> <?=$recupJoueur['prenom']?></td>
					</tr>
					<tr>
						<td><?=$rolenote['role']?></td>
					</tr>
					<tr>
						<td><?=$rolenote['note']?>/5</td>
					</tr>
				</table>
			<?php
			}
		}
	}
}			?>

	</table>
</div>
</body>
</html>